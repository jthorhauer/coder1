#!/bin/bash
source $HOME/.bash_profile

branch=$1
createdDate=$2

die () {
    echo >&1 "$@"
    exit 1
}

[ "$#" -ge 2 ] || die "2 arguments required, 3rd argument optional, $# provided.
Please provide
  - a branch name
  - branch create date in the form of dd/mm/yyyy
  - a comma delimited list of email addreses if you want to email the results
ex: sh run.sh branch_feature_1234 03/14/2013 joe@yakabod.com,sarah@yakabod.com"

if [ ! -d "build" ]; then
  mkdir build
fi


# if there is no ./build/base dir then create it and check out the base
# of the branch
if [ ! -d "build/base" ]; then
  cd build
  mkdir base
  cd base

  echo "--------------------------------------------------------";
  echo "-- Preparing the base of the branch:$branch.";
  echo "--------------------------------------------------------";
  echo "checking out vnelib from base of branch...";
  if [ "$branch" == "HEAD" ]
  then
    cvs -Q co -D $createdDate vnelib
    echo "checking out vne from base of branch...";
    cvs -Q co -D $createdDate vne
  else
    cvs -Q co -r $branch -D $createdDate vnelib
    echo "checking out vne from base of branch...";
    cvs -Q co -r $branch  -D $createdDate vne
  fi

  cd ../../

  echo "--------------------------------------------------------";
  echo "-- Building metrics for base of the branch:$branch.";
  echo "--------------------------------------------------------";
  # clear pdepend cache
  rm -rf ~/.pdepend/*
  rm -rf ./build/base/*.*
  ./bin/pdepend.phar --suffix="inc,php" --summary-xml=./build/base/vnelib-metric-results.xml --ignore="CSSMin.inc,JSMin.inc" ./build/base/vnelib/src/php/
  rm -rf ~/.pdepend/*
  ./bin/pdepend.phar --suffix="inc,php" --summary-xml=./build/base/vne-metric-results.xml --ignore="CSSMin.inc,JSMin.inc" ./build/base/vne/src/php/

  ./bin/phpcpd.phar --names="*.inc,*.php" --min-lines=10 --names-exclude="CSSMin.inc,JSMin.inc" ./build/base/vne/src/php > ./build/base/vne-copy-paste-detect-results.txt
  ./bin/phpcpd.phar --names="*.inc,*.php" --min-lines=10 --names-exclude="CSSMin.inc,JSMin.inc" ./build/base/vnelib/src/php > ./build/base/vnelib-copy-paste-detect-results.txt

  # compile the metrics for the base of the branch
  touch ./build/base/results.txt
  php ./bin/parseMetrics.php ./build/base > ./build/base/results.txt
fi



cd build

if [ ! -d "tip" ]; then
  mkdir tip
else
  rm -rf tip/*
fi

echo "--------------------------------------------------------";
echo "-- Preparing the tip of the branch:$branch.";
echo "--------------------------------------------------------";

# checkout tip of branch
cd tip
echo "checking out vnelib from tip of branch...";
cvs -Q co -r $branch vnelib
echo "checking out vne from tip of branch...";
  cvs -Q co -r $branch vne

cd ../../

echo "--------------------------------------------------------";
echo "-- Building metrics for tip of the branch:$branch.";
echo "--------------------------------------------------------";
# clear pdepend cache
rm -rf ~/.pdepend/*
rm -rf ./build/tip/*.*
./bin/pdepend.phar --suffix="inc,php" --summary-xml=./build/tip/vnelib-metric-results.xml --ignore="CSSMin.inc,JSMin.inc" ./build/tip/vnelib/src/php/
rm -rf ~/.pdepend/*
./bin/pdepend.phar --suffix="inc,php" --summary-xml=./build/tip/vne-metric-results.xml --ignore="CSSMin.inc,JSMin.inc" ./build/tip/vne/src/php/

./bin/phpcpd.phar --names="*.inc,*.php" --min-lines=10 --names-exclude="CSSMin.inc,JSMin.inc" ./build/tip/vne/src/php > ./build/tip/vne-copy-paste-detect-results.txt
./bin/phpcpd.phar --names="*.inc,*.php" --min-lines=10 --names-exclude="CSSMin.inc,JSMin.inc" ./build/tip/vnelib/src/php > ./build/tip/vnelib-copy-paste-detect-results.txt

touch ./build/tip/results.txt
php ./bin/parseMetrics.php ./build/tip > ./build/tip/results.txt

echo "--------------------------------------------------------";
echo "-- Generating metric comparison between base and tip of the branch:$branch.";
echo "--------------------------------------------------------";
touch ./build/comparison-results.txt
php ./bin/compareMetrics.php ./build/base ./build/tip > ./build/comparison-results.txt

if [ $# -eq 3 ]
then
    echo "--------------------------------------------------------";
    echo "-- Emailing report to $3";
    echo "--------------------------------------------------------";
    php ./bin/sendMetrics.php $branch ./build $3
fi


# create a zip of the report files
formattedDate=${createdDate//\//-}
echo "--------------------------------------------------------";
echo "-- Zipping metric results into ./build/code-debt-$formattedDate.zip";
echo "--------------------------------------------------------";
zip ./build/code-debt-$branch.zip  ./build/tip/* -i \*.txt -i \*.xml ;  zip report  ./build/base/* -i \*.txt -i \*.xml ;  zip report  ./build/* -i \*.txt  ;
