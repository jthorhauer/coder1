#!/bin/bash


die () {
    echo >&1 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "1 argument required.
Please provide
  - a comma delimited list of email addresses if you want to email the results
ex: sh runReport.sh  joe@yakabod.com,sarah@yakabod.com"


# compile the metrics for the base of the branch
touch ./build/base/results.txt
php ./bin/parseMetrics.php ./build/base > ./build/base/results.txt

echo "--------------------------------------------------------";
echo "-- Preparing metrics for tip of branch";
echo "--------------------------------------------------------";


touch ./build/tip/results.txt
php ./bin/parseMetrics.php ./build/tip > ./build/tip/results.txt

echo "--------------------------------------------------------";
echo "-- Generating metric comparison between base and tip of the branch.";
echo "--------------------------------------------------------";
touch ./build/comparison-results.txt
php ./bin/compareMetrics.php ./build/base ./build/tip > ./build/comparison-results.txt

if [ $# -eq 1 ]
then
    echo "--------------------------------------------------------";
    echo "-- Emailing report to $3";
    echo "--------------------------------------------------------";
    php ./bin/sendMetrics.php $branch ./build $1
fi

# create a zip of the report files
echo "--------------------------------------------------------";
echo "-- Zipping metric results into ./build/code-debt.zip";
echo "--------------------------------------------------------";
zip ./build/code-debt.zip  ./build/tip/* -i \*.txt -i \*.xml ;  zip report  ./build/base/* -i \*.txt -i \*.xml ;  zip report  ./build/* -i \*.txt  ;
