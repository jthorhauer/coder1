Here are some brief instructions for collecting debt metrics.

In order to get the Code Debt Index run the following command:
   ./run.sh [branch name] [branch start date] [(optional) email addresses]
   ex ./run.sh branch_feature_1234 03/14/2013 joe@yakabod.com,sarah@yakabod.com

The run.sh script will do the following:

+ if the base (beginning) of the branch has not been analyzed, check it out and analyze it
+ checkout the tip of the branch and analyze it
+ create a comparison report between base and tip of branch
+ if emails are provided send the results in an email

-------------------------------------
-- Build output files of interest
-------------------------------------
build/base/results.txt - The summary of for the base of the branch (start values)
build/tip/results.txt - The summary of code debt for the tip of the branch (currrent values)
build/comparison-results.txt - A diff of the items contributing to code debt from start to end of branch

-------------------------------------
-- File Descriptions
-------------------------------------
./run.sh - runs code debt generation
./clean.sh - deletes the contents of ./build dir.  run this if you have a previous branch in the build dir.

./lib/phpdepend.phar - code analyzer library to generate metrics
./lib/phpcpd.phar - copy paste detection library

./bin/parseMetrics.php - generate metrics using the phpdepend and phpcpd libraries
./bin/compareMetrics.php - generate report showing changes between base and tip of branch
./bin/sendMetrics.php - send email with the results of the parsing and the comparison report.
