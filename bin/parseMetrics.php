<?php

if (count($argv) < 2)
{
  echo "USAGE: php parseMetrics.php [dir]\n";
  exit(0);
}


try
{
  $baseDir = $argv[1].'/';

  $npathMax = 200;
  $elocMax = 130;

  $cpdVNELibFile  = $baseDir.'vnelib-copy-paste-detect-results.txt';
  $cpdVNEFile     = $baseDir.'vne-copy-paste-detect-results.txt';
  $vneLibFile     = $baseDir.'vnelib-metric-results.xml';
  $vneFile        = $baseDir.'vne-metric-results.xml';
  $vneLibNPathCnt = getMetric($vneLibFile, $searchAttr = 'npath', $threshold = $npathMax);
  $vneLibELOCCnt  = getMetric($vneLibFile, $searchAttr = 'eloc', $threshold = $elocMax);
  $vneNPathCnt    = getMetric($vneFile, $searchAttr = 'npath', $threshold = $npathMax);
  $vneELOCCnt     = getMetric($vneFile, $searchAttr = 'eloc', $threshold = $elocMax);
  $cpdVNECnt      = getCPDCount($cpdVNELibFile);
  $cpdVNELibCnt   = getCPDCount($cpdVNEFile);

  echo "\n----------------------------------------------";
  echo "\n For more info on metrics see:";
  echo "\n    http://pdepend.org/documentation/software-metrics/index.html";
  echo "\n    http://codingswag.ghost.io/cyclomatic-and-npath-complexity-explained/";
  echo "\n    https://github.com/sebastianbergmann/phpcpd";
  echo "\n";
  echo "\nMaximum NPath Complexity per method:$npathMax";
  echo "\nMaximum Executable Lines of Code per method (eloc):$elocMax";
  echo "\n----------------------------------------------";
  echo "\n";
  echo "\nVNE Lib Methods that exceed npath max:$vneLibNPathCnt";
  echo "\nVNE Lib Methods that exceed eloc max:$vneLibELOCCnt";
  echo "\n";
  echo "\nVNE Methods that exceed npath max:$vneNPathCnt";
  echo "\nVNE Methods that exceed eloc max:$vneELOCCnt";
  echo "\n";
  echo "\nVNE Instances of Copy/Paste Detection:$cpdVNECnt";
  echo "\nVNE Lib Instances of Copy/Paste Detection:$cpdVNELibCnt";
  echo "\n----------------------------------------------";
  echo "\nTotal Code Debt Index:".($vneLibNPathCnt + $vneLibELOCCnt + $vneNPathCnt + $vneELOCCnt + $cpdVNECnt + $cpdVNELibCnt);
  echo "\n----------------------------------------------";
  echo "\n";

}
catch( Exception $e )
{
  die( 'Error occurred : ' . $e->getMessage() );
}

  function getCPDCount($inputFile)
  {
    $count = 0;
    $handle = fopen($inputFile, "r");
    if ($handle)
    {
      while (($line = fgets($handle)) !== false)
      {
        if (strpos($line, 'Found') > -1)
        {
          $aParts = explode(" ", $line);
          $count = $aParts[1];
          break;
        }
        // process the line read.
      }
    }
    else
    {
      echo "\n!!!!!!!!!!!! ERROR Opening File:$inputFile\n";
    }
    fclose($handle);

    return $count;
  }

  function getMetric($inputFile, $searchAttr, $threshold)
  {
    $oDomDocument = new DOMDocument( '1.0', 'utf-8' );
    if( $oDomDocument->load($inputFile ))
    {
      $oXPath = new DOMXPath( $oDomDocument );

      $aMethodResults = getMethodOffenders($oXPath, $searchAttr, $threshold);
      $aFuncResult    = getFunctionOffenders($oXPath, $searchAttr, $threshold);
      $aCombined = array_merge($aMethodResults, $aFuncResult);
      usort($aCombined, 'sortByOrder');

      $contents = "------------------------------------\n";
      $contents .= "-- Results for $searchAttr \n";
      $contents .= "------------------------------------\n";
      foreach ($aCombined as $result)
      {
        $contents .= $result['name']."\n";
      }

      $filename = substr($inputFile,0,(strlen($inputFile)-3))."txt";
      if (is_file($filename))
      {
        $oldContents = file_get_contents($filename);
        $contents    = $oldContents."\n".$contents;
      }

      file_put_contents($filename, $contents);
      return count($aCombined);
    }
  }

  function sortByOrder($a, $b)
  {
    return $b['deficit'] - $a['deficit'];
  }

  function getMethodOffenders($oXPath, $searchAttr, $threshold)
  {
    $searchString  = '//method[@'.$searchAttr.'>'.$threshold.']';
    $oNodeList     = $oXPath->query($searchString);
    $aOffenderList = array();

    foreach ($oNodeList as $oNode)
    {
      if ($oNode)
      {
        $aOffenderList[] = getFullMethodName($oNode, $searchAttr, $threshold);
      }
    }
    return $aOffenderList;
  }

  function getFullMethodName($oNode, $searchAttr, $threshold)
  {
    $methodName = $oNode->getAttribute("name");
    $className  = $oNode->parentNode->getAttribute("name");
    $checkVal   = $oNode->getAttribute($searchAttr);
    $damageCnt  = $checkVal - $threshold;
    return array('deficit' => $checkVal,
                 'name'    =>$className."->".$methodName."() [+$checkVal]");
  }

  function getFunctionOffenders($oXPath, $searchAttr, $threshold)
  {
    $searchString  = '//function[@'.$searchAttr.'>'.$threshold.']';
    $oNodeList     = $oXPath->query($searchString);
    $aOffenderList = array();

    foreach ($oNodeList as $oNode)
    {
      if ($oNode)
      {
        $aOffenderList[] = getFullFunctionName($oXPath, $oNode, $searchAttr, $threshold);
      }
    }
    return $aOffenderList;
  }

  function getFullFunctionName($oXPath, $oNode, $searchAttr, $threshold)
  {
    $methodName    = $oNode->getAttribute("name");

    $childFileNodes = $oXPath->query("file", $oNode);
    $oFileNode =  $childFileNodes->item(0);

    $fullPathName = $oFileNode->getAttribute("name");
    $checkVal     = $oNode->getAttribute($searchAttr);
    $damageCnt    = $checkVal - $threshold;
    $aPathParts   = explode("/", $fullPathName);
    $className    = $aPathParts[(count($aPathParts)-1)];

    return array('deficit' => $checkVal,
                 'name'    =>$className."->".$methodName."() [+$checkVal]");
  }
?>
