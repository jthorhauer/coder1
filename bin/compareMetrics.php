<?php

if (count($argv) < 3)
{
  echo "USAGE: php metricCompare.php [old dir] [new dir]\n";
  exit(0);
}


try
{

  //01-20-2015-code-debt.tgz  result-summary.txt  vne  vne-copy-paste-detect-results.txt  vnelib  vnelib-copy-paste-detect-results.txt  vnelib-metric-results.txt  vnelib-metric-results.xml  vne-metric-results.txt  vne-metric-results.xml

  $newDir = $argv[2].'/';
  $oldDir = $argv[1].'/';

  // get the results of comparing the old vs new npath and eloc metric results
  parseMetricResults($newDir, $oldDir, $aNPathResults, $aElocResults, $aLibNPathResults, $aLibElocResults);

  // print the results
  printMetricResults($aNPathResults, $metricType = 'npath', $codeLocation = 'vne');
  printMetricResults($aElocResults, $metricType = 'eloc', $codeLocation = 'vne');
  printMetricResults($aLibNPathResults, $metricType = 'npath', $codeLocation = 'vnelib');
  printMetricResults($aLibElocResults, $metricType = 'eloc', $codeLocation = 'vnelib');

  // get the results of comparing the old vs new copy paste metric results
  parseCopyPasteResults($newDir, $oldDir, $aResults, $aLibResults);
  // print the results
  printMetricResults($aResults, $metricType = 'cpd', $codeLocation = 'vne');
  printMetricResults($aLibResults, $metricType = 'cpd', $codeLocation = 'vnelib');


}
catch( Exception $e )
{
  die( 'Error occurred : ' . $e->getMessage() );
}

function printMetricResults($aMetricResults, $metricType, $codeLocation)
{
  $sResults = getReportHeader($metricType, $codeLocation);

  foreach ($aMetricResults as $resultType => $aResults)
  {
    $sResults .= "$resultType\n";
    $cnt = 0;
    foreach ($aResults as $method => $score)
    {
      $sResults .= "\t$method  $score\n";
      $cnt++;
    }

    if ($cnt === 0)
    {
      $sResults .= "\tNONE\n";
    }
  }

  echo $sResults;
}

function getReportHeader($metricType, $codeLocation)
{
  $header = "------------------------------------------------------------\n";
  if ($metricType == 'npath')
  {
    if ($codeLocation == 'vne')
    {
      $header .= "-- VNE npath debt metrics. npath > 200\n";
    }
    else if ($codeLocation == 'vnelib')
    {
      $header .= "-- VNELIB npath debt metrics. npath > 200\n";
    }

    $header .= "-- The integer is the npath value for the method.\n";
  }
  else if ($metricType == 'eloc')
  {
    if ($codeLocation == 'vne')
    {
      $header .= "-- VNE eloc debt metrics. eloc max > 130\n";
    }
    else if ($codeLocation == 'vnelib')
    {
      $header .= "-- VNELIB eloc debt metrics. eloc max > 130\n";
    }

    $header .= "-- The integer is the number of lines in the method.\n";
  }
  else if ($metricType == 'cpd')
  {
    if ($codeLocation == 'vne')
    {
      $header .= "-- VNE copy paste debt metrics. max duplicate lines = 10\n";
    }
    else if ($codeLocation == 'vnelib')
    {
      $header .= "-- VNELIB copy paste debt metrics. max duplicate lines = 10\n";
    }

    $header .= "-- The integer is the number of copies detected in the file.\n";
  }

  $header .= "------------------------------------------------------------\n";
  return $header;
}

function parseMetricResults($newDir, $oldDir, &$aNPathResults, &$aElocResults, &$aLibNPathResults, &$aLibElocResults)
{
  $aNewVNEMetric    = file($newDir . 'vne-metric-results.txt');
  $aOldVNEMetric    = file($oldDir . 'vne-metric-results.txt');

  $aNewVNELibMetric = file($newDir . 'vnelib-metric-results.txt');
  $aOldVNELibMetric = file($oldDir . 'vnelib-metric-results.txt');

  $aNPathNew    = array();
  $aNPathOld    = array();
  $aElocNew    = array();
  $aElocOld    = array();

  $aLibNPathNew = array();
  $aLibNPathOld = array();
  $aLibElocNew = array();
  $aLibElocOld = array();

  // get npath and eloc metrics for both vne and vne lib for the old metrics
  parseArray( $aOldVNEMetric, $aNPathOld, $aElocOld );
  parseArray( $aOldVNELibMetric, $aLibNPathOld, $aLibElocOld );

  // get npath and eloc metrics for both vne and vne lib for the new metrics
  parseArray( $aNewVNEMetric, $aNPathNew, $aElocNew );
  parseArray( $aNewVNELibMetric, $aLibNPathNew, $aLibElocNew );

  // compare old and new for NPath and Eloc
  $aNPathResults = doCompare( $aNPathNew, $aNPathOld );
  $aElocResults = doCompare( $aElocNew, $aElocOld );

  $aLibNPathResults = doCompare( $aLibNPathNew, $aLibNPathOld );
  $aLibElocResults = doCompare( $aLibElocNew, $aLibElocOld );


}

function parseCopyPasteResults($newDir, $oldDir, &$aResults, &$aLibResults)
{
  // get results of comparing old vs new VNE copy paste metrics
  $aNewVNECPD       = file($newDir . 'vne-copy-paste-detect-results.txt');
  $aOldVNECPD       = file($oldDir . 'vne-copy-paste-detect-results.txt');

  $aNewCPDResults = array();
  $aOldCPDResults = array();
  parseCPDArray($aNewVNECPD, $aNewCPDResults);
  parseCPDArray($aOldVNECPD, $aOldCPDResults);
  $aResults = doCompare( $aNewCPDResults, $aOldCPDResults );

  // get results of comparing old vs new VNELIB copy paste metrics
  $aNewVNELibCPD    = file($newDir . 'vnelib-copy-paste-detect-results.txt');
  $aOldVNELibCPD    = file($oldDir . 'vnelib-copy-paste-detect-results.txt');

  $aNewLibCPDResults = array();
  $aOldLibCPDResults = array();
  parseCPDArray($aNewVNELibCPD, $aNewLibCPDResults);
  parseCPDArray($aOldVNELibCPD, $aOldLibCPDResults);
  $aLibResults = doCompare( $aNewLibCPDResults, $aOldLibCPDResults );
}

/**
 * Parse the copy paste detection file array.  Fill the
 * aCPD array with the counts for each file listed
 */
function parseCPDArray( $aArray, &$aCPD)
{
  foreach( $aArray as $sLine )
  {
    $sLine = str_replace('  -', '', $sLine);
    $sLine = str_replace(' ', '', $sLine);
    $sLine = str_replace("\t", '', $sLine);

    if (strpos($sLine, '/') === 0)
    {
      $aPathParts = explode('/', $sLine);
      $sFilePart = $aPathParts[count($aPathParts)-1];
      $aFileParts = explode(':', $sFilePart);
      $sFileName = $aFileParts[0];

      // if file is already there increment it
      if (array_key_exists($sFileName, $aCPD))
      {
        $val = ++$aCPD[$sFileName];
        $aCPD[$sFileName] = $val;
      }
      else
      {
        $aCPD[$sFileName] = 1;
      }
    }
  }
}

function parseArray( $aArray, &$aNPath, &$aEloc )
{
  // keep track of which section we are in
  $aCurrentResult = &$aNPath;

  foreach( $aArray as $sLine )
  {
    // headers for each section start with --
    if( substr( $sLine, 0, 2 ) === '--' )
    {
      continue;
    }
    //  a blank line splits the NPath and Eloc sections
    elseif( empty( trim( $sLine ) ) )
    {
      unset( $aCurrentResult );
      // change to our Eloc section
      $aCurrentResult = &$aEloc;
    }
    else
    {
      // split lines on space
      $aPieces = explode( ' ', $sLine );

      if( sizeof( $aPieces ) !== 2 )
      {
        throw new Exception( '$aPieces should have size of 2: ' . $sLine );
      }

      // the score is wrapped in [+1234]
      // remove the wrap
      $sScore = $aPieces[ 1 ];
      $sScore = substr( $sScore, 2, strlen( $sScore ) - 4 );

      $aCurrentResult[ $aPieces[ 0 ] ] = $sScore;
    }
  }
}

function doCompare( $aNew, $aOld )
{
  $aResults = array();

  $aNewMethods = array_keys( $aNew );
  $aOldMethods = array_keys( $aOld );

  // simple array diffs show us what was added/removed
  $aAddedMethods = array_diff( $aNewMethods, $aOldMethods );
  $aRemovedMethods = array_diff( $aOldMethods, $aNewMethods );

  $aAddedErrors = array();
  $aRemovedErrors = array();

  // foreach added/removed method - record the score
  foreach( $aAddedMethods as $sMethod )
  {
    $aAddedErrors[ $sMethod ] = $aNew[ $sMethod ];
  }

  foreach( $aRemovedMethods as $sMethod )
  {
    $aRemovedErrors[ $sMethod ] = $aOld[ $sMethod ];
  }

  $aFixedMethods = array();
  $aBrokenMethods = array();

  // go through the methods to find which scores changed
  foreach( $aNewMethods as $sMethod )
  {
    // if score is in both files
    if( isset( $aOld[ $sMethod ] ) )
    {
      $nOld = $aOld[ $sMethod ];
      $nNew = $aNew[ $sMethod ];

      if( $nOld < $nNew )
      {
        $aBrokenMethods[ $sMethod ] = $nOld . ' => ' . $nNew;
      }
      elseif( $nOld > $nNew )
      {
        $aFixedMethods[ $sMethod ] = $nOld . ' => ' . $nNew;
      }
    }
  }

  $aResults = array(
    'DEBT ADDED' => $aAddedErrors,
    'DEBT REMOVED' => $aRemovedErrors,
    'DEBT DECREASE' => $aFixedMethods,
    'DEBT INCREASE' => $aBrokenMethods,
  );

  return $aResults;
}
