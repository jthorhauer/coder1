<?php

if (count($argv) < 4)
{
  echo "USAGE: php sendMetrics.php [branch name] [build dir] [recipient list]\n";
  echo "  Ex: php sendMetrics.php branch_test_feature_123 ./build user1@yakabod.com,user2@yakabod.com,user3@yakabod.com\n";
  exit(0);
}

$branchName = $argv[1];
$buildDir   = $argv[2];
$to         = $argv[3];
$subject    = 'Code Debt Report for branch:'.$branchName.' on: ';
$headers    = 'From: jthorhauer@yakabod.com' . "\r\n" .
    'Reply-To: jthorhauer@yakabod.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

try
{
  $body         = "";
  $subject      .= date("Y-m-d h:i:sa");
  $baseFileName = $buildDir."/base/results.txt";
  $tipFileName  = $buildDir."/tip/results.txt";
  $compFileName = $buildDir."/comparison-results.txt";

  if (is_file($baseFileName))
  {
    $body .= "=============================================\n";
    $body .= "== B R A N C H    B A S E\n";
    $body .= "=============================================";
    $body .= file_get_contents($baseFileName);
    $body .= "\n\n";
  }

  if (is_file($tipFileName))
  {
    $body .= "=============================================\n";
    $body .= "== B R A N C H    T I P\n";
    $body .= "=============================================";
    $body .= file_get_contents($tipFileName);
    $body .= "\n\n";
  }

  if (is_file($compFileName))
  {
    $body .= "=============================================\n";
    $body .= "== C O D E    D E B T    C O M P A R I S O N \n";
    $body .= "=============================================\n";
    $body .= file_get_contents($compFileName);
  }

echo "sending mail\nto:$to\nsubject:$subject\nheaders:$headers\nbody:$body"  ;
  mail($to, $subject, $body, $headers);
}
catch( Exception $e )
{
  die( 'Error occurred : ' . $e->getMessage() );
}

?>
